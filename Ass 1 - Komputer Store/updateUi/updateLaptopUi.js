const titleElement = document.getElementById("h1-laptopTitle")
const descriptionElement = document.getElementById("p-laptopDescription")
const priceElement = document.getElementById("span-price")
const stockElement = document.getElementById("span-stock")
const imgElement = document.getElementById("img-laptopImage")
const laptopsElement = document.getElementById("select-laptops")
const featuresElement = document.getElementById("ul-laptopFeatures")

/**
 *  Populates the {@link laptopsElement} and update the html elements to show the data for the first object in the array
 * @param {array} laptops Array containing laptop objects
 */
const init = (laptops) => {
    updateLaptopElements(laptops[0])
    laptops.forEach(laptop => addLaptopToMenu(laptop))
}

/**
 *  Removes current children from {@link featuresElement} and appends new li-elements 
 * @param {object} laptop Laptop object
 */
const showFeatures = (laptop) => {
    while (featuresElement.firstChild) {
        featuresElement.firstChild.remove()
    }
    laptop.specs.forEach(spec => {
        const featureItem = document.createElement("li")
        featureItem.innerText = `${spec}`
        featuresElement.appendChild(featureItem)
    })
}
/**
 *  Creates a option-element and appends it to {@link laptopsElement}
 * @param {object} laptop Laptop object
*/
const addLaptopToMenu = (laptop) => {
    const laptopOptionElement = document.createElement("option")
    laptopOptionElement.value = laptop.id
    laptopOptionElement.appendChild(document.createTextNode(laptop.title))
    laptopsElement.appendChild(laptopOptionElement)
}

/**
 *  Changes the value of the disabled attribute and the innerText of {@link buyBtn}
 * @param {boolean} isDisabled Show or hide element
 * @param {string} text Text to display
 */
const toggleBuyBtn = (isDisabled, text) => {
    const buyBtn = document.getElementById("btn-buyLaptop")
    buyBtn.disabled = isDisabled
    buyBtn.innerText = text
}

/**
 *  Updates the innerText values of the nessesary html elements
 * @param {object} laptop Laptop object
 */
const updateLaptopElements = (laptop) => {
    showFeatures(laptop)
    titleElement.innerText = laptop.title
    descriptionElement.innerText = laptop.description
    priceElement.innerText = new Intl.NumberFormat('sv-SV', { style: 'currency', currency: 'SEK'  }).format(laptop.price)
    stockElement.innerText = laptop.stock
    imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptop.image

    laptop.stock <= 0 ? toggleBuyBtn(true, "Out of Stock") : toggleBuyBtn(false, "Buy now")
}

export { init, updateLaptopElements }