import { updatePaycheckElements, updateSavingsAndLoanElements } from "./updateUi/updateBankAndWorkUi.js"
import { init, updateLaptopElements } from "./updateUi/updateLaptopUi.js"
import Laptops from "./modules/laptops.js"
import Bank from "./modules/bank.js"
import Work from "./modules/work.js"

const laptopsElement = document.getElementById("select-laptops")
const repayLoanBtn = document.getElementById("btn-repayLoan")
const transferBtn = document.getElementById("btn-transfer")
const loanBtn = document.getElementById("btn-loan")
const workBtn = document.getElementById("btn-work")
const buyBtn = document.getElementById("btn-buyLaptop")

const bank = Bank()
const work = Work()
const laptops = Laptops()

// Fetches and handle the data about the laptops
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops.setLaptops(data))
    .then(() => {

        //Fixes a broken image url
        const laptop = laptops.getLaptopById(5)
        laptop.image = laptop.image.replace("jpg", "png")

        init(laptops.getLaptops())
        updatePaycheckElements(work.getPaycheck())
        updateSavingsAndLoanElements(bank.getBankBalance(), bank.getLoanBalance())
    })
    .catch(err => console.log(err))

// Uses work methods to increase paycheck amount and updates the nessesary html elemets 
workBtn.addEventListener("click", () => {
    work.goWork()
    updatePaycheckElements(work.getPaycheck())
})

// Uses bank and work methods to decrease the loan amount, resets the paycheck amount and updates the nessesary html elemets
repayLoanBtn.addEventListener("click", () => {
    bank.repayLoan(work.getPaycheck())
    work.resetPaycheck()
    updatePaycheckElements(work.getPaycheck())
    updateSavingsAndLoanElements(bank.getBankBalance(), bank.getLoanBalance())
})

// Uses bank and work methods to increase the bank amount by the value of the paycheck, resets the paycheck amount and updates the nessesary html elemets
transferBtn.addEventListener("click", () => {
    bank.transferToBank(work.getPaycheck())
    work.resetPaycheck()
    updatePaycheckElements(work.getPaycheck())
    updateSavingsAndLoanElements(bank.getBankBalance(), bank.getLoanBalance())
})


// Uses bank method to try and increase the values of loanBalance and bankBalance
loanBtn.addEventListener("click", () => {
    bank.applyForLoan()
    updateSavingsAndLoanElements(bank.getBankBalance(), bank.getLoanBalance())
})

// Uppdates the html elements that displays the laptop info with the data from the newly selected laptop
laptopsElement.addEventListener("change", (e) => {
    const selectedLaptop = laptops.getLaptopById(parseInt(e.target.value))
    updateLaptopElements(selectedLaptop)
})

// Uses bank method to decrease the bank balance and decreasing the "stock" value of the bought laptop
buyBtn.addEventListener("click", () => {
    const currentLaptop = laptops.getLaptopById(parseInt(laptopsElement.value))
    if (bank.getBankBalance() >= currentLaptop.price) {

        laptops.decreaseStockAmount(currentLaptop.id)
        updateLaptopElements(currentLaptop)

        bank.withdrawFromBankBalance(currentLaptop.price)
        updateSavingsAndLoanElements(bank.getBankBalance(), bank.getLoanBalance())

        alert(`Congratulations! You are now the owner of a: ${currentLaptop.title}`)
    } else {
        alert("You dont have enought money in your bank to buy this computer")
    }
})