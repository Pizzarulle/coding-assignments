
//Contains an array that stores all laptop objects. Returnes some functions that can be used to interact with the the data
const Laptops = () => {
    const laptops = []

    /**
     * Returns {@link laptops}
     * @returns {array} Array containing objects
     */
    const getLaptops = () => laptops

    /**
     * Populates array with the elemets from another array
     * @param {array} newLaptops  Array contining the data that will be pushed to a new array
     */
    const setLaptops = (newLaptops) => {
        newLaptops.forEach(currentLaptop => laptops.push(currentLaptop))
    }
    /**
     * Return one object by its id.
     * @param {int} id id 
     * @returns {object} One laptop object
     */
    const getLaptopById = (id) => laptops.filter(item => item.id === id)[0]

   /**
    * Decrease the stock value in a specific laptop object
    * @param {int} laptopId Id of the laptop 
    */
    const decreaseStockAmount = (laptopId) => {
        const laptop = getLaptopById(laptopId)
        laptop.stock -= 1
    }
    return {
        setLaptops,
        getLaptopById,
        decreaseStockAmount,
        getLaptops
    }
}

export default Laptops
