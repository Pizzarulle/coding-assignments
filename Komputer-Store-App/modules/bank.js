const Bank = () => {
    let bankBalance = 0
    let loanBalance = 0

    /**
     * Set the value of {@link loanBalance} to 0
     */
    const resetLoan = () => {
        loanBalance = 0
    }

    /**
     * Decreases the value of {@link loanBalance}
     * @param {int} amount Decrease amount
     */
    const payLoan = (amount) => {
        loanBalance -= amount
    }

    /**
     * Inceases the value of {@link bankBalance}
     * @param {int} amount Incease amount
     */
    const bankTransaction = (amount) => {
        bankBalance += amount
    }

    /**
    * Inceases the value of {@link loanBalance}
     * @param {int} amount Incease amount
     */
    const takeLoan = (amount) => {
        loanBalance += amount
    }

    /**
     * Gets the value of {@link bankBalance}
     * @returns {int} Value of  {@link bankBalance}
     */
    const getBankBalance = () => bankBalance

    /**
     * Decreases the value of {@link bankBalance}
     * @param {int} amount Decrease amount
     */
    const withdrawFromBankBalance = (amount) => bankTransaction(-amount)


    /**
     * Changes the values of {@link bankBalance} and {@link loanBalance} depending on if the values of the current {@link loanBalance} is 0 or not
     * @param {int} int Decrease/increase amount
     */
    const transferToBank = (amount) => {
        if (loanBalance != 0) {
            let deducuionAmount = amount * 0.1
            let remainingPaycheck = amount - deducuionAmount
            if (loanBalance - deducuionAmount >= 0) {
                payLoan(deducuionAmount)
                bankTransaction(remainingPaycheck)
            }
            else {
                amount -= loanBalance
                bankTransaction(amount)
                resetLoan()
            }
        }
        else {
            bankTransaction(amount)
        }
    }
    /**
     * Returns the value of {@link loanBalance}
     * @returns {int} Value of {@link loanBalance}
     */
    const getLoanBalance = () => loanBalance

    /**
     * Decreases the value of {@link loanBalance} to a minimum of zero. 
     * @param {int} amount Decrease amount
     */
    const repayLoan = (amount) => {
        if (amount > loanBalance) {
            bankTransaction(amount - loanBalance)
            resetLoan()
        }
        else {
            payLoan(amount)
        }
    }

    /**
     * Increases the {@link loanBalance} and {@link bankBalance} values if current value of {@link loanBalance} is zero. 
     * Increase amount can not be more than doubble the {@link bankBalance} current value
     */
    const applyForLoan = () => {
        if (bankBalance != 0) {
            if (loanBalance != 0) {
                alert("You cannot get more than one bank loan before repaying the last loan!")
            } else {
                let amount
                while (true) {
                    amount = parseInt(prompt("Enter the amount you want to loan (must be a number and greater than 0)!"))

                    if (isNaN(amount)) {
                        return
                    }
                    if (amount >= 0) {
                        break
                    }
                }
                if (amount > bankBalance * 2) {
                    alert("You cannot get a loan more than double the amount of your bank balance")
                } else {
                    takeLoan(amount)
                    bankTransaction(amount)
                }
            }
        } else {
            alert("You have no money on your bank. Therefor you can not get a loan.")
        }
    }
    return {
        getBankBalance,
        withdrawFromBankBalance,
        transferToBank,
        getLoanBalance,
        repayLoan,
        applyForLoan
    }
}

export default Bank
