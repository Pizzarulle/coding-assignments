# Assignment 1 - Komputer Store App

Simple HTML and JavaScript page where you can interact with different elements to get money, transfer money, apply for a loan, look at different computers and their specs and finally buy the computer that you liked (if you have enough money in the bank).

## Installation

Open index.html file with Live Server

## Usage
Use the work button to generate som money.
Transfer that money to the bank.
Take out a loan.
Browse different computers and buy them if there's enough money in the bank

## Contributors
[@AHells](https://www.gitlab.com/pizzarulle)

## License
